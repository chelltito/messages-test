const assert = require("assert");
const path = require("path");
const os = require("os");

const Application = require("spectron").Application;
const electronPath = require("electron");

// TODO: this assumes OSX for now
const appDir = `messages-${os.platform()}-${os.arch()}/messages.app/Contents/Resources/app`;

describe("an initial test for the Messages application", function () {
  this.timeout(10000);

  beforeEach(async function () {
    this.app = new Application({
      path: electronPath,
      args: [path.join(__dirname, "..", appDir)],
    });

    await this.app.start();
  });

  afterEach(async function () {
    if (this.app && this.app.isRunning()) {
      await this.app.stop();
    }
  });

  // This could be a single test but I've broken them up for this example.
  // We would also want to get much more in depth with assertions and validation
  // assertions after updates, but this is just a very quick proof of concept.
  it("launches the Messages window", async function () {
    const count = await this.app.client.getWindowCount();
    assert.equal(count, 1);
  });

  it("validates the title", async function () {
    const title = await this.app.client.getTitle();
    assert.equal(title, "Messages for web");
  });

  it("clicks the user memory toggle", async function () {
    const toggle = await this.app.client.$(".mat-slide-toggle-thumb");
    const hiddenInput = await this.app.client.$("#mat-slide-toggle-1-input");

    assert.equal(await hiddenInput.isSelected(), false);
    await toggle.click();
    assert.equal(await hiddenInput.isSelected(), true);
  });
});
