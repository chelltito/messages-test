# Messages-Test

A sample demo to show a proof of concept for automated testing of the Android Messages web application, wrapped in Electron. This uses Mocha and Spectron, which uses WebDriverIO under the hood.

## Installation

```
npm install
```

This will install all dependencies as well as generate a prebuilt version of the Android Messages electron app.

## Running

```
npm test
```
